import pathLib from "path"
import del from "del"

// import ImageQueue from "../../"

// const imageQueue = new ImageQueue();

import WorkerPool from "../../WorkerPool";
const workerPool = new WorkerPool(__NB_WORKERS__, "python3", ["VehiclesDetectionPython/testing/carDetectionInference.py"])
// const workerPool = new WorkerPool(__NB_WORKERS__, "python", ["VehiclesDetectionPython/testing/carDetectionInference.py"])
// const workerPool = new WorkerPool(__NB_WORKERS__, "node", ["dummy.js"])

export default (job) => {
    const {
        image,
    } = job.data.params;

    console.log("VehiclesDetection (" + job.id + ")");

    const t = Date.now();
    // console.log(image)

    // Workpool example
    return workerPool.push(image.path + "\n\n\n\n")
    .then((data) => {
        console.log(data + " -- " + (Date.now() - t) + " ms");
        // console.log(JSON.parse(data));
    })
    .catch((err) => {
        console.log("ERROR IN WORKER POOL :".red)
        console.error(err)
    })


    // return del([image.path], { force: true })
    // .then(() => {
    //     console.log(`[${job.id}] : IMAGE DELETE`)
    // })
    // .catch((err) => {
    //     console.error(`[${job.id}] : `, err)
    // });
    // TODO
}
