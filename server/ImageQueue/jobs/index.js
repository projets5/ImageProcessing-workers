
// Import jobs
import VehiclesDetection from "./VehiclesDetection"
// Dict of jobs availaible
const jobs = {
    VehiclesDetection,
}

// Run the right job base on type
export default (job) => jobs[job.data.type](job);
