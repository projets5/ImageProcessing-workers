from time import sleep
import time
import tensorflow as tf
#from contextlib import closing
#from videosequence import VideoSequence as vid
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import test_detection
from detectionWorker import Worker


############################
# Constants
############################
REJECTION_THRESHOLD = 0.45
IM_PATH = "images/car_"


def loadImage(index):
    im = np.array(Image.open(IM_PATH+str(index).zfill(3)+".jpg"),dtype=np.uint8)
    return im

def getRectangle(boxes,height,width):
    x1 = boxes[1]*width
    y1 = boxes[0]*height
    x2 = boxes[3]*width
    y2 = boxes[2]*height
    print("boxes : {} {} {} {}".format(x1,y1,x2,y2))
    rect = patches.Rectangle( (x1,y1),(x2-x1),(y2-y1), linewidth=1, edgecolor='r', facecolor= 'none')
    return rect

def applyRectangleAndShow(image,rectangle,scores):
    fig, ax = plt.subplots(1)
    ax.imshow(image)
    ax.add_patch(rectangle)
    print("score = ",scores)
    plt.show()
    return

def applyRectanglesAndShow(image, rectangles, scores):
    fig, ax = plt.subplots(1)
    ax.imshow(image)
    for r in rectangles:
        ax.add_patch(r)
    [print("score = ", score) for score in scores]
    plt.show()
    return

def main(show=False):
    carDetector = test_detection.postitDetector()
    start = time.time()

    for i in range(0,130): # increments of 2
        im1 = loadImage(i)
        t1 = time.time()
        boxes, scores, classes, num = carDetector.get_detection(im1)
        t2 = time.time()
        print("time for inference: ", t2-t1)
#        if scores[0][0] < REJECTION_THRESHOLD: 
#            print("### image rejected, score = {}".format(scores[0][0]) )
#            continue
        print("boxes raw : ",boxes)
        print("number of car detected: {}".format(num[0]))
        
        shape = im1.shape
#        print("shape = ",shape)
        rectangles = []
        for car,score in zip(boxes[0],scores[0]):
            if score >= REJECTION_THRESHOLD:
                rectangles.append(getRectangle(car, shape[0], shape[1]))
            else:
                continue
        if show:
            #rect = getRectangle(car,shape[0], shape[1])
            applyRectanglesAndShow(im1, rectangles, scores)
    end = time.time()
    print("=============  total time ==============\n{}".format((end-start)))


if __name__ == "__main__":
    main(True)
