import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import sys
import numpy as np
from PIL import Image
import time


############################
REJECTION_THRESHOLD = 0.45
############################

class postitDetector(object):
    def __init__(self):
        PATH_TO_MODEL = os.path.dirname(os.path.abspath(__file__)) + '/fine_tuned_model/frozen_inference_graph.pb'
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_MODEL, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
            self.d_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
            self.d_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
            self.d_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_d = self.detection_graph.get_tensor_by_name('num_detections:0')

        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.035)
        self.sess = tf.Session(
            graph=self.detection_graph,
            config=tf.ConfigProto(gpu_options=gpu_options)
        )

    def get_detection(self, img):
        with self.detection_graph.as_default():
            img_expanded = np.expand_dims(img, axis=0)
            (boxes, scores, classes, num) = self.sess.run(
                [self.d_boxes, self.d_scores, self.d_classes, self.num_d],
                feed_dict={self.image_tensor: img_expanded})
        return boxes, scores, classes, num

def loadImage(path):
    try:
        im = np.array(Image.open(path.rstrip('\n')),dtype=np.uint8)
    except:
        raise Exception("image loading error, path is {}".format(path))
    return im


def printResults(boxes, scores, classes, num):
    # first iteration of program only needs number of cars detected
    car_nbr = 0
    for score in scores[0]:
        if score >= REJECTION_THRESHOLD:
            car_nbr += 1

    # Send data back (stdout)
    print(car_nbr)
    sys.stdout.flush()

def listen(detector):
    while(True):
        line = sys.stdin.readline().replace("\\n", "").replace("\"", "")
        if line is "":
            continue

        image = loadImage(line)
        boxes, scores, classes, num = detector.get_detection(image)
        printResults(boxes, scores, classes, num)


def main():
    # initialize tensorflow session and load inference graph
    detector = postitDetector()
    listen(detector)
    sys.exit()


if __name__ == "__main__":
    main()
