import { spawn } from "child_process"

// Singleton
let workerPoolSingleton = null;
export default class WorkerPool {
    constructor(nb_worker, cmd, args) {
        if(!workerPoolSingleton) {
            workerPoolSingleton = this;
            this._workers = [];
            this._cmd = cmd;
            this._args = args;


            for(var w=0; w<nb_worker; w++) {
                this._createWorker();
            }
        }

        return workerPoolSingleton;
    }

    _createWorker() {
        // Setup a worker
        const w = spawn(this._cmd, this._args, {
            cwd: __PROCESSES_FOLDER__, // Expose scripts in the __PROCESSES_FOLDER__ folder
        });

        console.log(`SPAWNED (${w.pid})`)

        // TODO auto start new worker if it's the case ?
        w.on("error", (err) => {
            console.error(`ERROR (${w.pid})`, err);
        })
        w.on("exit", (err, sig) => {
            console.error(`EXITED (${w.pid})`, err, sig);
        })

        process.on("exit", () => {
            w.kill();
        })

        w.stderr.on("data", (err) => {


            console.log(String(err));
            // rej(JSON.stringify(err));
        })

        w.stdout.on("data", (result) => {

            console.log(String(result));
            // res(JSON.stringify(err));
        })

        this._workers.push(w);


        // // Worker is ready
        // this._freeWorker(w);
    }

    _freeWorker(w) {
        // Called when the worker is ready for new task
        w.stderr.removeAllListeners();
        w.stdout.removeAllListeners();

        this._workers.push(w);
    }

    push(data) {
        const w = this._workers.pop();
        // TODO test if this._workers is empty, but it should never be the case ?

        if(!w) {
            console.log("NO WORKER AVAILABLE")
            return false;
        }

        return new Promise((res, rej) => {
            w.stdin.write(JSON.stringify(data) + "\n");
            // w.stdin.flush();

            w.stderr.on("data", (err) => {
                // worker is dead, we need to create a new one
                w.kill()
                this._createWorker();

                rej(String(err));
                // rej(JSON.stringify(err));
            })

            w.stdout.on("data", (result) => {
                this._freeWorker(w);

                res(String(result));
                // res(JSON.stringify(err));
            })
        })
    }

}
