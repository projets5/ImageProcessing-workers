FROM tensorflow/tensorflow:1.6.0-devel-gpu-py3

# Install node prereqs, nodejs and yarn
# Ref: https://deb.nodesource.com/setup_8.x
# Ref: https://yarnpkg.com/en/docs/install
RUN \
  apt-get update && \
  apt-get install -yqq apt-transport-https
RUN \
  echo "deb https://deb.nodesource.com/node_8.x jessie main" > /etc/apt/sources.list.d/nodesource.list && \
  wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
  apt-get update && \
#   apt-get install -yqq nodejs build-essential gcc && \
  apt-get install -yqq nodejs && \
  pip install Pillow && \
  rm -rf /var/lib/apt/lists/*

# RUN npm i -g npm
RUN npm i -g nodemon
# RUN npm i -g gulp

# Install python packages
# COPY ./server/WorkerPool/processes/VehiclesDetectionPython /var/VehiclesDetectionPython
# RUN /var/VehiclesDetectionPython/installPackages.sh
